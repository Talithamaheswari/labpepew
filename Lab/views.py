from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from Lab.forms import Todo_Form
from Lab.models import Todo

from django.http import HttpResponseForbidden, JsonResponse
from django.shortcuts import render
import json
import requests
from django.shortcuts import redirect
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout

todo = []
response = {}
def index(request):
    html = 'index.html'
    todo = Todo.objects.all()
    response['todo'] = todo
    response['todo_form'] = Todo_Form
    return render(request, html, response)

def add_todo(request):
    if(request.method == 'POST'):
        response['title'] = request.POST['title']
        response['description'] = request.POST['description']
        todo = Todo(title=response['title'],description=response['description'])
        todo.save()
        return HttpResponseRedirect(reverse('index'))
    else:
        return HttpResponseRedirect(reverse('index'))

def about(request):
    return render(request,'about.html')

def library(request):
    return render(request, 'library.html')

def search(request):
    url = 'https://www.googleapis.com/books/v1/volumes'
    url +=  f"?maxResults={10}&q={request.GET.get('q')}"
    data = requests.get(url).json()
    return JsonResponse(data, json_dumps_params={'indent': 2})



def login(request):
    if request.user.is_authenticated:
        return redirect('index')
    else:
        if request.method == "POST":
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(
                request, username=username, password=password
            )
            if user is not None:
                auth_login(request, user)
                request.session['username'] = user.username
                return redirect('index')
            else:
                return render(
                    request, 'login.html', {
                        'error' : 'invalid credential'
                    }
                )

    return render(request, 'login.html')

def logout(request):
    auth_logout(request)
    return redirect('index')


# Create your views here.
