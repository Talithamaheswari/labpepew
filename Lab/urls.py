from django.conf.urls import url
from django.urls import *

from .views import *
#url for app
urlpatterns = [
    path('', index, name='index'),
    url(r'^add_todo/$', add_todo, name='add_todo'),
    path('about/', about, name= "about"),
    path('library/', library, name='library'),
    path('library/volumes/',search, name='search'),
    path('login/', login, name='login'),
    path('logout/', logout, name='logout')

]
