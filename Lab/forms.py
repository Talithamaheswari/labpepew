from django import forms
from .models import *

class Todo_Form(forms.Form):
    attrs = {
        'class' : 'form-control'
        }

    title = forms.CharField(label='Title', required=True, max_length=27, widget=forms.TextInput(attrs=attrs))
    description = forms.CharField(label='Description', required=True, widget=forms.Textarea(attrs=attrs))

    class Meta:
        model = Todo
