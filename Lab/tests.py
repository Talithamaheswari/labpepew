from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve, reverse
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from django.contrib.auth.models import User

from .views import index, add_todo, login
from .models import Todo
from .forms import Todo_Form
import time


# Create your tests here.
class Lab6UnitTest(TestCase):

    def test_lab_6_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_lab6_using_index_func(self):
        found = resolve(reverse('index'))
        self.assertEqual(found.func, index)

    def test_model_can_create_new_todo(self):
        # Creating a new activity
        new_activity = Todo.objects.create(title='mengerjakan lab ppw', description='mengerjakan lab_6 ppw')

        # Retrieving all available activity
        counting_all_available_todo = Todo.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)

    def test_form_todo_input_has_placeholder(self):
        form = Todo_Form()
        self.assertIn('id="id_title"', form.as_p())
        self.assertIn('id="id_description', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = Todo_Form(data={'title': '', 'description': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['description'],
            ["This field is required."]
        )


    def test_lab6_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/add_todo', {'title': '', 'description': ''})
        self.assertEqual(response_post.status_code, 301)

        response = Client().get('')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)


class Lab6FunctionalTest(TestCase):

    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--window-size=1420,1080')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.selenium = webdriver.Chrome(chrome_options=chrome_options)


    def tearDown(self):
        self.selenium.quit()
        super(Lab6FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/')
        # find the form element
        title = selenium.find_element_by_id('id_title')
        description = selenium.find_element_by_id('id_description')

        submit = selenium.find_element_by_id('submit')

        # Fill the form with data
        title.send_keys('Mengerjakan Lab PPW')
        description.send_keys('Lab kali ini membahas tentang CSS dengan penggunaan Selenium untuk Test nya')

        # submitting the form
        submit.send_keys(Keys.RETURN)

        time.sleep(10)
        self.assertIn("Mengerjakan Lab PPW",self.selenium.page_source)

    class BooksTestCase(TestCase):
        def test_books_url_exists(self):
            response = self.client.get(reverse('books:books'))
            self.assertEqual(response.status_code, 200)

        def test_books_using_about_func(self):
            found = resolve(reverse('books:books'))
            self.assertEqual(found.func, views.books)
        def test_books_using_about_template(self):
            response = self.client.get(reverse('books:books'))
            self.assertTemplateUsed(response, 'books.html')


class Story_9_Unit_Test(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user = User.objects.create_user('talitha', 'talitha@gmail.com', 'test123')
        cls.user.first_name = 'talitha'
        cls.user.save()

    def test_login_exist(self):
        response = self.client.get(reverse('login'))
        self.assertEqual(response.status_code, 200)

    def test_login_using_login_func(self):
        found = resolve('/login/')
        self.assertEqual(found.func, login)

    def test_login_using_login_template(self):
        response = self.client.get(reverse('login'))
        self.assertTemplateUsed(response, 'login.html')

        self.assertContains(response, 'Username')
        self.assertContains(response, 'Password')

    def test_login_logged_in_is_redirect(self):
        self.client.login(username='talitha', password='test123')
        response = self.client.get(reverse('login'))
        self.assertEqual(response.status_code, 302)

    def test_login_not_logged_in(self):
        response = self.client.get(reverse('login'))
        html = response.content.decode()
        self.assertIn('<form', html)

    def test_login_submit(self):
        response = self.client.post(
            reverse('login'), data={
                'username': 'talitha',
                'password': 'test123',
            }
        )

        self.assertEqual(response.status_code, 302)
        response = self.client.get(reverse('index'))
        html = response.content.decode()
        self.assertIn(self.user.username, html)

    def test_login_logout(self):
        response = self.client.post(
            reverse('login'), data={
                'username': 'talitha',
                'password': 'test123',
            }
        )

        self.client.get(reverse('logout'))
        response = self.client.get(reverse('index'))
        html = response.content.decode()
        self.assertNotIn(self.user.username, html)

# Create your tests here.
